#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

void err( const char* msg ) {
    fprintf(stderr, "%s%s",msg,"\n");
}

extern inline size_t size_max( size_t x, size_t y );
